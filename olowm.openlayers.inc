<?php

/**
 * @file
 * Provides hooks for integration with OpenLayers (http://drupal.org/project/openlayers)
 */

/**
 * Implements hook_openlayers_maps().
 */
function olowm_openlayers_maps() {

  $layers_key = _olowm_layers_info();
  $layers = array('mapquest_openaerial' => 'mapquest_openaerial');
  foreach ($layers_key as $key) {
    $name = 'olowm_layer_' . $key;
    $layers[$name] = $name;
  }

  $map = new stdClass();
  $map->api_version = 1;
  $map->name = 'olowm_example_map';
  $map->title = t('OpenWeatherMap Example Map');
  $map->description = t('A Map Used for displaying weather informations.');
  $map->disabled = TRUE;
  $map->data = array(
    'width' => '100%',
    'height' => '600px',
    'image_path' => drupal_get_path('module', 'openlayers') . '/themes/default_dark/img/',
    'css_path' => drupal_get_path('module', 'openlayers') . '/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '0, 0',
        'zoom' => '1',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'default_layer' => 'mapquest_openaerial',
    'layers' => $layers,
    'behaviors' => array(
      'openlayers_behavior_layerswitcher' => array(),
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 0,
        'panIcons' => 1,
      ),
    ),
    'projection' => 'EPSG:900913',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default',
      'temporary' => 'default',
    ),
    'map_name' => 'olowm_example_map',
  );

  return array(
    'olowm_example_map' => $map,
  );
}


/**
 * Formatter layers
 */
function olowm_openlayers_layers() {
  $layers = array();

  foreach (_olowm_layers_info() as $key) {
    $url = 'http://${s}.tile.openweathermap.org/map/' . $key . '/${z}/${x}/${y}.png';
    $openlayers_layers = new stdClass;
    $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
    $openlayers_layers->api_version = 1;
    $openlayers_layers->name = 'olowm_layer_' . $key;
    $openlayers_layers->title = 'OpenWeatherMap layer' . $key;
    $openlayers_layers->description = 'http://openweathermap.org/';
    $openlayers_layers->disabled = TRUE;
    $openlayers_layers->data = array(
      'layer_type' => 'openlayers_layer_type_xyz',
      'url' => array(
        0 => $url,
      ),
      'isBaseLayer' => FALSE,
      'opacity' => 0.5,
      'projection' => array('EPSG:900913'),
    );
    $layers[$key] = $openlayers_layers;
  }

  return $layers;
}

function _olowm_layers_info() {
  return array(
    'precipitation',
    'precipitation_cls',
    'rain',
    'rain_cls',
    'snow',
    'clouds',
    'clouds_cls',
    'pressure',
    'pressure_cntr',
    'temp',
    'wind',
  );
}
