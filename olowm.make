api = 2
core = 7.x

libraries[openweathermap][download][type] = "file"
libraries[openweathermap][download][url] = "http://openweathermap.org/js/OWM.OpenLayers.1.3.4.js"
libraries[openweathermap][directory_name] = openweathermap
libraries[openweathermap][destination] = "libraries"
