/**
 * OpenLayers OpenWeatherMap Handler
 */
Drupal.openlayers.layer.owm = function(title, map, options) {
  var layeroptions = {
    drupalID: options.drupalID
  }

  return new OpenLayers.Layer.Vector[options.type](title, layeroptions);
};
